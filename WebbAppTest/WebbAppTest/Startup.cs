﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebbAppTest.Startup))]
namespace WebbAppTest
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
